﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TC3
{
    class Nodo
    {
        public string nombre;
        public string id;
        public int puntos; 
        public Nodo siguiente;
        public Nodo anterior;
    }
}
