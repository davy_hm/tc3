﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TC3
{
    class ListaCircularDoble
    {
        public void RecorrerLista(Nodo final)
        {
            Console.WriteLine("Inicio de la lista");
            Nodo actual;
            if (final == null)
            {
                Console.WriteLine("La lista está vacía");
                return;
            }
            actual = final.siguiente;
            do
            {
                Console.WriteLine(actual.nombre);
                actual = actual.siguiente;
            } while (actual != final.siguiente);
            Console.WriteLine("fin de la lista");
            Console.WriteLine();
        }
        public Nodo AgregarAListaVacia(Nodo fin, string nombre, string id, int puntos)
        {
            Console.WriteLine("Agregando información de nuevo jugador...\nID: " +id+" Nombre: "+nombre+" Puntos: "+puntos);
            Nodo temporal = new Nodo() { nombre = nombre, id = id, puntos = puntos };
            fin = temporal;
            fin.siguiente = fin;
            fin.anterior = fin;
            return fin;
        }

        public Nodo AgregarInicio(Nodo fin, string nombre, string id, int puntos)
        {
            if (fin==null)
            {
                Console.WriteLine("Lista vacia\n");
                AgregarAListaVacia(fin, nombre, id, puntos);
            }
            Console.WriteLine("Agregando información del jugador "+nombre+" al inicio de la lista");
            Nodo temp = new Nodo() { nombre = nombre, id = id, puntos = puntos };
            temp.siguiente = fin.siguiente;
            temp.anterior = fin;
            fin.siguiente.anterior = temp;
            fin.siguiente = temp;
            return fin;
        }
        public Nodo AgregarFinal(Nodo fin,string nombre,string id, int puntos)
        {
            if (fin==null)
            {
                Console.WriteLine("Lista vacia\n");
                AgregarAListaVacia(fin, nombre, id, puntos);
            }
            Console.WriteLine("Agregando informacion del jugador "+nombre+" al final de lista...");
            Nodo temp = new Nodo() { nombre = nombre, id = id, puntos = puntos };
            temp.siguiente = fin.siguiente;
            temp.anterior = fin.anterior;
            fin.siguiente = temp;
            fin.siguiente.anterior = temp;
            fin = temp;
            return fin;
        }
        //public Nodo Agregar
    }
}
